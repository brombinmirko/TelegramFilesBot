#!/usr/bin/env python
# -*- coding: utf-8 -*-  

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from telegram.error import TelegramError, Unauthorized, BadRequest,TimedOut, ChatMigrated, NetworkError
import logging, os

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Configuration
default_path = "/home/YOUR_USERNAME"
bot_token = "YOUR_BOT_TOKEN"

def error(bot, update, error):
    logger.warning('{log}: Update "%s" --- ERROR --- "%s"', update, error)

def start(bot, update):
  update.message.reply_text('Ciao!\nUsa /help per cominciare!')

def help_text(bot, update):
  update.message.reply_text('/list per la lista dei file\n/get [path] per scaricare un file')

def list_my_files(bot, update):
  files = ""
  for f in os.listdir(default_path):
    files = files+f+"\n"
  update.message.reply_text(files)

def get_file(bot, update):
  file = update.message.text[5:]
  bot.send_document(chat_id=update.message.chat_id, document=open(file, 'rb'))

def main():
    updater=Updater(bot_token)
    dp = updater.dispatcher
    dp.add_error_handler(error)
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help_text))
    dp.add_handler(CommandHandler("list", list_my_files))
    dp.add_handler(CommandHandler("get", get_file))
    updater.start_polling()
    updater.idle()

if __name__=='__main__':
    main()
